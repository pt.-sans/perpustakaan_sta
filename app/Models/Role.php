<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use app\Traits\Uuids;

class Role extends Model
{
    use HasFactory, Uuids;

    protected $table = 'roles';
}