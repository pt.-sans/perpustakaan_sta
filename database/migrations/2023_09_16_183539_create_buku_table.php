<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('judul');
            $table->uuid('id_penerbit')->constrained();
            $table->uuid('id_pengarang')->constrained();
            $table->uuid('id_kategori')->constrained();
            $table->date('tgl_masuk');
            $table->string('harga_buku');
            $table->string('slug');
            $table->timestamps();

            $table->foreign('id_kategori')->references('id')->on('kategori');
            $table->foreign('id_penerbit')->references('id')->on('penerbit');
            $table->foreign('id_pengarang')->references('id')->on('pengarang');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('buku');
    }
};