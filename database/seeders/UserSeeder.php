<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('users')->insert([
            'id' => Str::uuid(),
            'name' => 'Andrew',
            'email' => 'andrewdefretes@gmail.com',
            'password' => '$2y$10$RpEuD1QZ9mpHkxToH.vesO1Py9pb14HSGdVJgCedFiLECN3v9Wc8O',
            'id_role' => '65f21178-cd9c-4280-b3cd-e8132adb7443',
        ]);
    }
}