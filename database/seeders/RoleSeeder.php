<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use app\Models\Role;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('roles')->insert([
            'id' => Str::uuid(),
            'role' => 'admin',
        ]);

        \DB::table('roles')->insert([
            'id' => Str::uuid(),
            'role' => 'operator',
        ]);

    }
}